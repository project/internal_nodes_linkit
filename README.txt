INTRODUCTION
------------

Integration of linkit with internal nodes, so we don't need to find the node
to get the nid, it will gives you a functionality to get you raw url.

REQUIREMENTS
------------
* Internal nodes (http://drupal.org/project/internal_nodes)
* linkit (3.x) (https://www.drupal.org/project/linkit)


CONFIGURATION
-------------

1. CREATE NEW PROFILE

   create new profile of linkit module and if you want to use exist you can
   use the existing profile

2. GO TO NODE TYPE EDIT PAGE.

   On node edit page when you goto Internal Nodes Settings tab and check the
   "Enable Linkit for 301 redirect text field. " to use linkit. and select the
   profile which you prefer.

3. CREATE / EDIT NODE.

   On node create or edit page you can see the "Search" button for
   301 - Redirect destination field. and you can click on search and that
   opens a popup to select the node from available nodes.

MAINTAINERS
-----------
Current maintainers:
 * Mitesh Patel (developermitesh) - https://www.drupal.org/u/developermitesh
This project has been sponsored by:
* COWORKS
  Specialized in development of Drupal powered sites,
  Visit http://www.coworks.be/ for more information.
