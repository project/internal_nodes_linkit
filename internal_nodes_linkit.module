<?php
/**
 * @file
 * Internal nodes linkit module file.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function internal_nodes_linkit_form_node_form_alter(&$form, &$form_state, $form_id) {
  // If per-node settings enabled.
  $node_type = $form['type']['#value'];

  if (variable_get('internal_nodes_nodes_' . $node_type, 0) && variable_get('internal_nodes_linkit_enable_' . $node_type, 0)) {
    $profile_name = variable_get('internal_nodes_linkit_profile_' . $node_type, 0);
    if (!empty($profile_name)) {
      // Load the profile.
      $profile = linkit_profile_load($profile_name);

      // Load the insert plugin for the profile.
      $insert_plugin = linkit_insert_plugin_load($profile->data['insert_plugin']['plugin']);

      // Set the field ID.
      $field_id = 'edit-internal-nodes-url';

      $field_js = array(
        'data' => array(
          'linkit' => array(
            'fields' => array(
              $field_id => array(
                'profile' => $profile->name,
                'insert_plugin' => $profile->data['insert_plugin']['plugin'],
                'url_method' => $profile->data['insert_plugin']['url_method'],
              ),
            ),
          ),
        ),
        'type' => 'setting',
      );

      $form['internal_nodes']['internal_nodes_url']['#attributes']['style'] = array('width:auto;');
      // Attach js files and settings Linkit needs.
      $form['internal_nodes']['internal_nodes_url']['#attached']['library'][] = array('linkit', 'base');
      $form['internal_nodes']['internal_nodes_url']['#attached']['library'][] = array('linkit', 'field');
      $form['internal_nodes']['internal_nodes_url']['#attached']['js'][] = $insert_plugin['javascript'];
      $form['internal_nodes']['internal_nodes_url']['#attached']['js'][] = $field_js;

      $form['internal_nodes']['internal_nodes_url']['#field_suffix'] = l(t('Search'), '', array(
          'attributes' => array(
            'class' => 'button linkit-field-button linkit-field-' . $field_id,
          ),
        )
      );
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function internal_nodes_linkit_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  // Enable Linkit on this field instance.
  $form['internal_nodes']['internal_nodes_linkit_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Linkit for 301 redirect text field.'),
    '#default_value' => variable_get('internal_nodes_linkit_enable_' . $form['#node_type']->type, 0),
    '#description' => t('Do not use this for CKeditor and TinyMCE fields.'),
  );

  $profiles = linkit_profile_field_load_all();
  $options = array();
  foreach ($profiles as $profile) {
    $options[$profile->name] = $profile->admin_title;
  }

  // Sort the options.
  natsort($options);

  $form['internal_nodes']['internal_nodes_linkit_profile'] = array(
    '#type' => 'select',
    '#title' => t('Profile'),
    '#options' => $options,
    '#empty_option' => t('- Select a profile -'),
    '#default_value' => variable_get('internal_nodes_linkit_profile_' . $form['#node_type']->type, 0),
    '#states' => array(
      'invisible' => array(
        'input[name="internal_nodes_linkit_enable"]' => array('checked' => FALSE),
      ),
      'required' => array(
        'input[name="internal_nodes_linkit_enable"]' => array('checked' => TRUE),
      ),
    ),
    '#element_validate' => array('internal_nodes_linkit_field_profile_validate'),
  );
}

/**
 * Validation callback; profile field if linkit is enabled on the instance.
 *
 * @see linkit_form_field_ui_field_edit_form_alter()
 */
function internal_nodes_linkit_field_profile_validate($element, &$form_state, $form) {
  if (isset($form_state['values']['internal_nodes_linkit_enable'])
    && $form_state['values']['internal_nodes_linkit_enable']) {
    if (empty($element['#value'])) {
      form_error($element, t('You must select an profile.'));
    }
  }
}
